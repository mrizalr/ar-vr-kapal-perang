using System;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    public int shipId;
    public List<string> stringTimes;
    public List<DateTime> times;

    public Vector3 initialLocation;
    public List<Vector3> geolocation;

    public void Init(int id, int max, Vector3 initialLocation)
    {
        shipId = id;
        var count = UnityEngine.Random.Range(max, max+11);

        this.initialLocation = initialLocation;

        times = GeneraterRandomTime(count);
        geolocation = GenerateRandomLocation(count);

        stringTimes = new List<string>();
        for (int i = 0; i < times.Count; i++)
        {
            stringTimes.Add(times[i].ToString());
        }

        GetComponent<ShipManager>().Init(this);
    }

    private List<Vector3> GenerateRandomLocation(int count)
    {
        var locations = new List<Vector3>();
        var currentLoc = initialLocation;

        for (int i = 0; i < count; i++)
        {
            var randomValue = new Vector3(UnityEngine.Random.Range(0, 3), 0, UnityEngine.Random.Range(0, 3));
            currentLoc = currentLoc + randomValue/10;
            locations.Add(currentLoc);
        }
        return locations;
    }

    private List<DateTime> GeneraterRandomTime(int count)
    {
        var times = new List<DateTime>();
        var generatedTime = DateTime.Now;
        generatedTime = generatedTime.AddHours(UnityEngine.Random.Range(-5, 5));
        for (int i = 0; i < count; i++)
        {
            times.Add(generatedTime);
            generatedTime = generatedTime.AddSeconds(600);
        }
        return times;
    }
}
