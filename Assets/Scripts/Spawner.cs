using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    public int shipCount;
    public int dataCount;
    public GameObject shipPrefabs;

    private List<GameObject> ships = new List<GameObject>();
    private APIRequest apiRequestLocation;

    void Awake()
    {
        apiRequestLocation = GetComponent<APIRequest>();
        apiRequestLocation.OnLoadData();
        for (int i = 0; i < shipCount; i++)
        {
            StartCoroutine(CreateShip(i));
        }

        StartCoroutine(SetSlider());
    }

    private IEnumerator SetSlider()
    {
        while (ships.Count != shipCount)
        {
            yield return new WaitForSeconds(.1f);
        }

        var timeSpan = FindObjectOfType<TimespanSlider>();
        timeSpan.Init(ships);
    }

    private IEnumerator CreateShip(int shipID)
    {
        while (apiRequestLocation.IsDataLoading())
        {
            yield return new WaitForSeconds(.1f);
        }

        var ship = Instantiate(shipPrefabs);

        var shipData = ship.GetComponent<Ship>();
        shipData.Init(shipID, dataCount, apiRequestLocation.GetLocation()[shipID]);

        var shipRenderer = ship.GetComponent<MeshRenderer>();
        shipRenderer.enabled = GameObject.Find("Area").GetComponent<MeshRenderer>().enabled ? true : false;
        shipRenderer.material.color = GenerateRandomColor();
        ship.transform.parent = GameObject.Find("ships").transform;
        ship.transform.localScale = new Vector3(5f, 5f, 5f);
        ship.transform.localPosition = shipData.geolocation[0];
        ship.transform.LookAt(shipData.geolocation[1]);
        ship.name = "ship " + shipID;

        ships.Add(ship);
    }

    private Color GenerateRandomColor()
    {
      return new Color(
          UnityEngine.Random.Range(0f, 1f),
          UnityEngine.Random.Range(0f, 1f),
          UnityEngine.Random.Range(0f, 1f)
          );
    }
}
