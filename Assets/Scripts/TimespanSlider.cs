using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimespanSlider : MonoBehaviour
{
    private Slider slider;
    private DateTime startTime;
    private DateTime endTime;
    private DateTime currentTime;
    private List<DateTime> closestTimes;
    private List<Ship> shipData = new List<Ship>();
    private List<ShipManager> shipManager = new List<ShipManager>();

    public void Init(List<GameObject> ships)
    {
        foreach (var ship in ships)
        {
            shipData.Add(ship.GetComponent<Ship>());
            shipManager.Add(ship.GetComponent<ShipManager>());
        }

        slider = GetComponent<Slider>();

        SetSliderRangeValue();
        FindClosestTimes();

        slider.onValueChanged.AddListener(delegate { FindClosestTimes(); });
    }

    public DateTime GetStartValue()
    {
        return startTime;
    }

    public DateTime GetCurrentTime()
    {
        return currentTime;
    }

    public List<DateTime> GetClosestTimes()
    {
        return closestTimes;
    }

    private void GetTimeRange()
    {
        var minTimes = new DateTime(9999, 12, 31);
        var maxTimes = new DateTime();
        foreach (var ship in shipData)
        {
            if (ship.times[ship.times.Count - 1] > maxTimes)
                maxTimes = ship.times[ship.times.Count - 1];
            if (ship.times[0] < minTimes)
                minTimes = ship.times[0];
        }

        startTime = minTimes;
        endTime = maxTimes;
    }

    private void SetSliderRangeValue()
    {
        GetTimeRange();
        var timeDiff = (endTime - startTime).TotalMinutes;

        slider.minValue = 0;
        slider.maxValue = (float)timeDiff;
    }

    private void FindClosestTimes()
    {
        currentTime = startTime.AddMinutes(slider.value);
        closestTimes = new List<DateTime>();

        for (int i = 0; i < shipData.Count; i++)
        {
            var times = shipData[i].times;
            var min = float.MaxValue;
            var closestTime = new DateTime();

            foreach (var time in times)
            {
                if (Mathf.Abs((float)(time - currentTime).TotalSeconds) < min)
                {
                    min = Mathf.Abs((float)(time - currentTime).TotalSeconds);
                    closestTime = time;
                }
            }
            closestTimes.Add(closestTime);
            shipManager[i].SetShipActiveStatus(currentTime, closestTime);
        }

        //string logMessage = currentTime.ToString() + " | ";
        //foreach (var time in closestTimes)
        //{
        //    logMessage += time.ToString() + " | ";
        //}

        //Debug.Log(logMessage);
    }
}
