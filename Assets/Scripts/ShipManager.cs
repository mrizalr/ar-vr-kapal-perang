using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipManager : MonoBehaviour
{
    private Ship shipData;
    private TimespanSlider timeSpan;
    private LineRenderer lr;

    private DateTime timeSelected;
    private int timesIndex;

    public void Init(Ship ship)
    {
        shipData = ship;
        timeSpan = FindObjectOfType<TimespanSlider>();
        lr = GetComponentInChildren<LineRenderer>();
    }

    private void Update()
    {
        SetLine(timesIndex);
    }

    public void SetShipActiveStatus(DateTime currentTime, DateTime closestTime)
    {
        var diff = (currentTime - closestTime).TotalMinutes;
        diff = Mathf.Abs((float)diff);

        if (diff > 10)
            gameObject.SetActive(false);
        else
            gameObject.SetActive(true);

        MoveShip();
    }

    private void MoveShip()
    {
        timeSelected = timeSpan.GetClosestTimes()[shipData.shipId];

        timesIndex = shipData.times.IndexOf(timeSelected);
        transform.localPosition = shipData.geolocation[timesIndex];
    }

    private void SetLine(int timesIndex)
    {
        var parentTransform = GameObject.Find("map").transform;

        lr.positionCount = timesIndex + 1;
        lr.material.color = shipData.GetComponent<MeshRenderer>().material.color;
        lr.widthMultiplier = parentTransform.localScale.x;
        for (int i = 0; i < lr.positionCount; i++)
        {
            var linePosition = shipData.geolocation[i];
            linePosition.y = .5f;
            var position = parentTransform.TransformPoint(linePosition);
            lr.SetPosition(i, position);
        }
        lr.Simplify(parentTransform.localScale.x);
    }
}
