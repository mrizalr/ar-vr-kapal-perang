using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using SimpleJSON;


public class APIRequest : MonoBehaviour
{
    public Text listText;
    [Range(1, 807)]
    public int dataCount = 1;

    private List<Vector3> coordinates;
    private bool isRunning;
    private float timeCount;
    private readonly string baseURL = "https://ipwhois.app/json/";

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            OnLoadData();
        }

        if (isRunning)
        {
            timeCount += Time.deltaTime;
        }
    }

    public void OnLoadData()
    {
        dataCount = FindObjectOfType<Spawner>().shipCount;
        listText.text = "";
        coordinates = new List<Vector3>();

        listText.text = "loading ... ";

        StartCoroutine(RequestFromAPI());
    }

    public bool IsDataLoading()
    {
        return isRunning;
    }

    public string GetRandomIpAddress()
    {
        return $"{Random.Range(1, 256)}.{Random.Range(1, 256)}.{Random.Range(1, 256)}.{Random.Range(1, 256)}";
    }

    IEnumerator RequestFromAPI()
    {
        isRunning = true;
        for (int i = 0; i < dataCount; i++)
        {
            var ip = GetRandomIpAddress();
            string url = baseURL + ip;
            UnityWebRequest infoRequest = UnityWebRequest.Get(url);

            yield return infoRequest.SendWebRequest();
            if (infoRequest.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log(infoRequest.error);
                yield break;
            }

            JSONNode dataInfo = JSON.Parse(infoRequest.downloadHandler.text);
            if (dataInfo["latitude"] != null && dataInfo["longitude"] != null)
            {
                var lat = dataInfo["latitude"];
                var lon = dataInfo["longitude"];

                Debug.Log("INFO - " + (i+1));
                Debug.Log(lat + "," + lon);
                Debug.Log("city - " + dataInfo["city"]);
                Debug.Log("=================");

                var x = (float)(lon / 3.6);
                var z = (float)(lat / 3);
                var newLoc = new Vector3(x, 0, z);
                coordinates.Add(newLoc);
            }
            else
            {
                i--;
            }
        }
        isRunning = false;
        listText.text = "";
        Debug.Log(timeCount);
    }

    public List<Vector3> GetLocation()
    {
        return coordinates;
    }
}
